<?php

$errorMSG = "";

// NAME
    if (empty($_POST["name"])) {
        $errorMSG = "Name is required ";
    } else {
        $name = $_POST["name"];
    }

    // EMAIL
    if (empty($_POST["email"])) {
        $errorMSG .= "Email is required ";
    } else {
        $email = $_POST["email"];
    }

    // MESSAGE
    if (empty($_POST["message"])) {
        $errorMSG .= "Message is required ";
    } else {
        $message = $_POST["message"];
    }

    $EmailTo = "davidkli313@gmail.com";
    $Subject = "New Message";

    // prepare email body text
    $Body = "";
    $Body .= "Name: ";
    $Body .= $name;
    $Body .= "\n";
    $Body .= "Email: ";
    $Body .= $email;
    $Body .= "\n";
    $Body .= "Message: ";
    $Body .= $message;
    $Body .= "\n";

    // send email

    $url = "https://www.google.com/recaptcha/api/siteverify";
  $data = [
    'secret' => "6Ld3QakUAAAAABVVhC4jOln25_affmvip7_KdhUo",
    'response' => $_POST['token'],
    // 'remoteip' => $_SERVER['REMOTE_ADDR']
  ];

  $options = array(
      'http' => array(
        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
        'method'  => 'POST',
        'content' => http_build_query($data)
      )
    );

  $context  = stream_context_create($options);
    $response = file_get_contents($url, false, $context);

  $res = json_decode($response, true);
  if($res['success'] == true) {

      $success = mail($EmailTo, $Subject, $Body, "From:".$email);

    // redirect to success page
    if ($success && $errorMSG == ""){
        echo json_encode(['code' => 200, 'message' => '<strong>Success!</strong> Your inquiry successfully submitted.']);
    }else{
        if($errorMSG == ""){
            echo json_encode(['code' => 500, 'message' => '<strong>Error!</strong> Something went wrong!.']);
        } else {
            echo json_encode(['code' => 500, 'message' => '<strong>Error!</strong> '. $errorMSG .'']);
        }
    }
      
  } else {
      echo json_encode(['code' => 500, 'message' => '<strong>Error!</strong> You are not a human.']);
  }
?>
