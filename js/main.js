

document.getElementById('contactForm').addEventListener('submitForm', submitForm);


function submitForm(e){
  var name = $("#name").val();
  var email = $("#email").val();
  var message = $("#message").val();
  saveMessage(name, email, message);

  document.querySelector('.alert').style.display = 'block';

  setTimeout(function(){
    document.querySelector('.alert').style.display = 'none';
  },3000);

  document.getElementById('contactForm').reset();
}

// function getInputVal(id){
//   return document.getElementById(id).value;
// }

function saveMessage(name, email, message){
  var newMessageRef = messagesRef.push();
  newMessageRef.set({
    name: name,
    email:email,
    message:message
  });
}
