$("#contactForm").validator().on("submit", function (event) {
    if (event.isDefaultPrevented()) {
        formError();
        submitMSG(false, "Did you fill in the form properly?");
    } else {
        event.preventDefault();
        submitForm();
    }
});

function submitForm(){
    var name = $("#name").val();
    var email = $("#email").val();
    var message = $("#message").val();

    grecaptcha.ready(function() {
        grecaptcha.execute('6Ld3QakUAAAAALYrmiUyhOfCSfWZbPcSk7WEccQs', {action: 'contactForm'}).then(function(token) {
           $.ajax({
            type: "POST",
            url: "php/form-process.php",
            data: {name: name, email: email, message: message, token: token},
            success : function(result){
                result = JSON.parse(result);
                console.log(result);
                if (result.code == 200){
                    formSuccess();
                } else {
                    formError();
                    submitMSG(false,result.message);
                }
            }
    });
        });
    });
}

function formSuccess(){
    $("#contactForm")[0].reset();
    submitMSG(true, "Message Submitted!")
}

function formError(){
    $("#contactForm").removeClass().addClass('shake animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
        $(this).removeClass();
    });
}

function submitMSG(valid, msg){
    if(valid){
        var msgClasses = "h3 text-center tada animated text-success";
    } else {
        var msgClasses = "h3 text-center text-danger";
    }
    $("#msgSubmit").removeClass().addClass(msgClasses).text(msg);
}
